﻿namespace StateMachines.Core
{
    public abstract class WorkflowFunction : WorkflowNode
    {
        public abstract void Evaluate();
    }
}