﻿using System;

namespace StateMachines.Core
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class InputAttribute: Attribute
    {
         
    }
}